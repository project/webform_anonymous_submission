CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Webform anonymous module gives webform option to submit 
the webform as anonymous. It unset the username and IP when webform is submitted.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/webform_anonymous_submission

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/webform_anonymous_submission


REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

1. Go to existing/new webform.
2. In settings -> form check "Anonymous submission" checkbox.
3. Save settings. 


MAINTAINERS
-----------

Current maintainers:
 * Druproid Support Inc.: https://druproidsupport.com
 * Shalin Shukla (skshukla96@gmail.com): /u/shalin_shukla
 * Gurjinder Singh (gurjinder_12@yahoo.com): /u/gurjinder_pabla
